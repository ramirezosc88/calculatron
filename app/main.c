#include "app_bsp.h"
#define SUM 1
#define SUB 2
#define MUL 3
#define DIV 4
#define MAX 2147483647
#define MIN -2147483648
/**------------------------------------------------------------------------------------------------
Brief.- Punto de entrada del programa
2 Modifique el programa anterior para que responda por serial si fue una letra o un numero
-------------------------------------------------------------------------------------------------*/
UART_HandleTypeDef UartHandle;
char RxBuffer[50];
uint8_t RxByte;
__IO ITStatus stat = RESET;

char *part1;// SUM 10 525
char *part2;
char *part3;
char str_res[15];

void UART_Init(void);
void print_Result(char *operator);
uint8_t search(char *str);
uint8_t valid_command(void);
uint8_t is_number(char *str);
int32_t str_to_int(char *str);
void int_to_str(int num, char *str);

int main(void)
{   
    HAL_Init();
    UART_Init();
    uint8_t selection;
    int32_t numA;
    int32_t numB;
    bool overflow = true;
    int32_t result;
    char *operator;
    char cpyRxBuf[40];

    HAL_UART_Receive_IT(&UartHandle, &RxByte, 1);

    while (1)
    {
        if (stat == SET)
        {
            stat = RESET;
            strcpy(cpyRxBuf, RxBuffer);
            part1 = strtok(cpyRxBuf, " "); // SUM 200 30
            part2 = strtok(NULL, " ");
            part3 = strtok(NULL, " ");
            
            if (valid_command())
            {    
                selection = search(part1);
                numA = str_to_int(part2);
                numB = str_to_int(part3);
                
                switch (selection)
                {
                case SUM:
                    result = numA + numB; 
                    if ( (numA > 0 && numB > 0 && result < 0) || (numA < 0 && numB < 0 && result > 0) ) // 127 + 2 -127
                    {
                        overflow = true;
                    }
                    else
                    {
                        overflow = false;       
                        operator = " + ";
                    }
                    break;
                case SUB:
                    result = numA - numB;
                    if ( ((numA < 0) && numB > 0 && result > 0) || (numA > 0 && numB < 0 && result < 0) ) 
                    {
                        overflow = true;
                    }
                    else
                    {
                        overflow = false;     
                        operator = " - ";
                    }
                    break;
                case MUL:
                    result = numA * numB;  
                    if ( (numA != 0 && result / numA != numB) ) // 100 * -3 = -300 ---- -300 / 100 = 3 = -3 
                    {
                        overflow = true;
                    }
                    else
                    {
                        overflow = false;    
                        operator = " * ";
                    }
                    break;
                case DIV:
                    if ( (numA == MIN && numB == -1) || numB == 0)
                    {
                        overflow = true;
                    }
                    else
                    {
                        overflow = false;
                        result = numA / numB;
                        operator = " / ";
                    }
                    break;        
                }         
                if (overflow)
                {
                    HAL_GPIO_TogglePin(GPIOA, 0x10);
                    HAL_UART_Transmit(&UartHandle, (uint8_t *)("ERROR\n"), (strlen("ERROR\n") ), 250);
                }
                else
                {
                    int_to_str(result, str_res);
                    print_Result(operator);
                }    
            }
            else
            {
                HAL_UART_Transmit(&UartHandle, (uint8_t *)("ERROR\n\r"), (strlen("ERROR\n\r") ), 250);
            }
        }
    }
    return 0u;
}


void print_Result(char *operator)
{
    char arr[50];
    strcpy(arr, part2);
    strcpy(&arr[strlen(arr)], operator);
    strcpy(&arr[strlen(arr)], part3);
    strcpy(&arr[strlen(arr)], " = ");
    strcpy(&arr[strlen(arr)], str_res);
    strcpy(&arr[strlen(arr)], "\n");
    HAL_UART_Transmit(&UartHandle, (uint8_t *)arr, strlen(arr), 500); 
}

uint8_t search(char *str)
{
    char *commands[] = {" ", "SUM", "SUB", "MUL", "DIV"};
    for (uint8_t i = 1; i < 5; i++)
    {
        if (strcmp(str, commands[i]) == 0)
        {
            return i;
        }
    }
    return 0;
}

uint8_t valid_command(void)
{
    if (search(part1) && is_number(part2) && is_number(part3))
    {
        return 1;
    }
    return 0;
}

void UART_Init(void)
{
    UartHandle.Init.BaudRate = 9600;
    UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
    UartHandle.Init.StopBits = UART_STOPBITS_1;
    UartHandle.Init.Parity = UART_PARITY_NONE;
    UartHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    UartHandle.Init.Mode = UART_MODE_TX_RX;
    UartHandle.Instance = USART2;
    UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;

    HAL_UART_Init(&UartHandle);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    static uint32_t i = 0;
    RxBuffer[i] = RxByte;
    i++;
    if (RxBuffer[i - 1] == '\r') // [1, 2, 5, E, *]
    {
        RxBuffer[i - 1] = '\0';
        stat = SET;
        i = 0;
    }
    HAL_UART_Receive_IT(&UartHandle, &RxByte, 1);
}

void int_to_str(int num, char *str)
{
    int base = 10;
    int i = 0;
    bool isNegative = false;
    char rev[100];
    uint8_t len;
    uint8_t count = 0;
    uint8_t j = 0;

    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return ;
    }
    if (num < 0)
    {
        isNegative = true;
        num = -num;
    }

    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
        num = num / base;
    }

    if (isNegative)
        str[i++] = '-';

    str[i] = '\0';

    while (str[count] != '\0')
    {
        count++;
    }

    len = count - 1;
    for (; j < count; j++)
    {
        rev[j] = str[len];
        len--;
    }

    strcpy(str, rev); //[1234] = 4321
    str[j ] = '\0';
}

int32_t str_to_int(char *str)
{
    int32_t num = 0;
    uint8_t i = 0;
    if (str[0] == '-')
    {
        i = 1;
    }
    for (; str[i] != '\0'; ++i)
    {
        num = num * 10 + str[i] - '0';
    }
    if (str[0] == '-')
    {
        num = -num;
    }
    return num; // -2,147,483,648    2,147,483,647
}

uint8_t is_number(char *str)
{
    uint8_t i = 0;
    uint8_t a;
    if (str[0] == '-')
    {
        i = 1;
    }
    for (; i < strlen(str); i++) //-125
    {
        if (str[i] >= '0' && str[i] <= '9')
        {
            a = 1;
        }
        else
        {
            a = 0;
            break;
        }
    }
    return a;
}
